# coding=utf-8
TAM = 10
t1_a, t2_a, t1_b, t2_b = {'lock-X': False, 'lock-S': False}, {'lock-X': False, 'lock-S': False}, {'lock-X': False,
                                                                                                  'lock-S': False}, {
                             'lock-X': False, 'lock-S': False}
t1_atrasada, t2_atrasada = False, False
instrucoes_atrasadas = list()


def ler_instrucoes():
    """
    Faça um programa que leia as instruções (lock-S, lock-X, read, write, unlock) de duas transações. Suponha
    que cada transação tem no máximo 10 instruções. As instruções devem ser lidas conforme a ordem de
    execução de uma determinada escala de execução concorrente, definida pelo usuário.
    Assuma que as transações manipulam o mesmo conjunto de dados, e que este conjunto tenha 2 elementos
    (por exemplo, A e B).
    A entrada de dados deve ter o seguinte formato:
    Transação: a que transação se refere à instrução que vai ser lida. Exemplo: T1
    Instrução: qual é a instrução lida. Exemplos: lock-X, read, write, unlock, lock-S
    Dado: sobre qual dado a instrução se refere. Exemplo: A (lock-S(A)), B (read(B)), C (unlock(C))
    """
    instrucoes = list()
    x = 0
    # Lê a entrada até 10 vezes
    while x < TAM:
        transacao = input("Qual transação será lida? ").upper()
        instrucao = int(input("Qual instrução será lida? 1 lock-X, 2 read, 3 write, 4 unlock, 5 lock-S: "))
        dado = input("Qual dado? A ou B: ").upper()

        instrucoes.append({'transacao': transacao, 'instrucao': instrucao, 'dado': dado})

        novo = input("Novo item? s/n ")
        if novo == 'n':
            x = TAM

        x += 1

    return instrucoes


def imprimir_instrucoes(instrucoes):
    for instrucao in instrucoes:
        print('Transação:', instrucao['transacao'])
        print('Instrução:', instrucao['instrucao'])
        print('Dado:', instrucao['dado'])
        print('\n')


def executa_instrucao_sem_print(instrucao, x):
    global t1_atrasada
    global t2_atrasada
    global t1_a
    global t2_a
    global t1_b
    global t2_b
    global instrucoes_atrasadas

    # INSTRUÇÕES DE T1
    if instrucao['transacao'] == 'T1':
        # T1 DADO A
        if instrucao['dado'] == 'A':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x na outra transação e insere em atrasada
                if t2_a['lock-X']:
                    t1_atrasada = True
                    return False
                else:
                    t1_a['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t1_a['lock-S'] is False:
                    t1_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t1_a['lock-X'] is False:
                    t1_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t1_a['lock-X'], t1_a['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t2_a['lock-X']:
                    t1_atrasada = True
                    return False
                else:
                    t1_a['lock-S'] = True

        # T1 DADO B
        elif instrucao['dado'] == 'B':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t2_b['lock-X']:
                    t1_atrasada = True
                    return False
                else:
                    t1_b['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t1_b['lock-S'] is False:
                    t1_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t1_b['lock-X'] is False:
                    t1_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t1_b['lock-X'], t1_b['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t2_b['lock-X']:
                    t1_atrasada = True
                    return False
                else:
                    t1_b['lock-S'] = True

    # INSTRUÇÕES DE T2
    elif instrucao['transacao'] == 'T2':
        # T2 DADOS A
        if instrucao['dado'] == 'A':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t1_a['lock-X']:
                    t2_atrasada = True
                    return False
                else:
                    t2_a['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t2_a['lock-S'] is False:
                    t2_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t2_a['lock-X'] is False:
                    t2_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t2_a['lock-X'], t2_a['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t1_a['lock-X']:
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-S'] = True

        # T2 DADOS B
        elif instrucao['dado'] == 'B':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t1_b['lock-X']:
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t2_b['lock-S'] is False:
                    t2_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t2_b['lock-X'] is False:
                    t2_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t2_b['lock-X'], t2_b['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t1_b['lock-X']:
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-S'] = True
    return True


def executa_instrucao(instrucao, x):
    global t1_atrasada
    global t2_atrasada
    global t1_a
    global t2_a
    global t1_b
    global t2_b
    global instrucoes_atrasadas

    # INSTRUÇÕES DE T1
    if instrucao['transacao'] == 'T1':
        # T1 DADO A
        if instrucao['dado'] == 'A':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x na outra transação e insere em atrasada
                if t2_a['lock-X']:
                    print('T1 está esperando A que já está bloqueado por T2 em %s' % x)
                    t1_atrasada = True
                    return False
                else:
                    t1_a['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t1_a['lock-S'] is False:
                    print('T1 não tem permissão para leitura de A em %s' % x)
                    t1_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t1_a['lock-X'] is False:
                    print('T1 não tem permissão para escrever de A em %s' % x)
                    t1_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t1_a['lock-X'], t1_a['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t2_a['lock-X']:
                    print('T1 está esperando A que já está bloqueado por T2 em %s' % x)
                    t1_atrasada = True
                    return False
                else:
                    t1_a['lock-S'] = True

        # T1 DADO B
        elif instrucao['dado'] == 'B':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t2_b['lock-X']:
                    print('T1 está esperando B que já está bloqueado por T2')
                    t1_atrasada = True
                    return False
                else:
                    t1_b['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t1_b['lock-S'] is False:
                    print('T1 não tem permissão para leitura de B em %s' % x)
                    t1_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t1_b['lock-X'] is False:
                    print('T1 não tem permissão para escrever de B em %s' % x)
                    t1_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t1_b['lock-X'], t1_b['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t2_b['lock-X']:
                    print('T1 está esperando B que já está bloqueado por T2 em %s' % x)
                    t1_atrasada = True
                    return False
                else:
                    t1_b['lock-S'] = True

    # INSTRUÇÕES DE T2
    elif instrucao['transacao'] == 'T2':
        # T2 DADOS A
        if instrucao['dado'] == 'A':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t1_a['lock-X']:
                    print('T2 está esperando o dado A que já está bloqueado por T1 em %s' % x)
                    t2_atrasada = True
                    return False
                else:
                    t2_a['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t2_a['lock-S'] is False:
                    print('T2 não tem permissão para leitura de A em %s' % x)
                    t2_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t2_a['lock-X'] is False:
                    print('T2 não tem permissão para escrever de A em %s' % x)
                    t2_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t2_a['lock-X'], t2_a['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t1_a['lock-X']:
                    print('T2 está esperando o dado A que já está bloqueado por T1 em %s' % x)
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-S'] = True

        # T2 DADOS B
        elif instrucao['dado'] == 'B':
            # LOCK-X
            if instrucao['instrucao'] == 1:
                # Verifica se há lock-x no dado na outra transação
                if t1_b['lock-X']:
                    print('T2 está esperando o dado B que já está bloqueado por T1 em %s' % x)
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-X'] = True

            # READ
            elif instrucao['instrucao'] == 2:
                # Verifica se há lock-S para leitura
                if t2_b['lock-S'] is False:
                    print('T2 não tem permissão para leitura de B em %s' % x)
                    t2_atrasada = True
                    return False

            # WRITE
            elif instrucao['instrucao'] == 3:
                if t2_b['lock-X'] is False:
                    print('T2 não tem permissão para escrever de B em %s' % x)
                    t2_atrasada = True
                    return False

            # UNLOCK
            elif instrucao['instrucao'] == 4:
                t2_b['lock-X'], t2_b['lock-S'] = False, False

            # LOCK-S
            elif instrucao['instrucao'] == 5:
                # Verifica se há lock-x na outra transação
                if t1_b['lock-X']:
                    print('T2 está esperando o dado B que já está bloqueado por T1 em %s' % x)
                    t2_atrasada = True
                    return False
                else:
                    t2_b['lock-S'] = True
    return True


def verifica_deadlock(instrucoes):
    global t1_atrasada
    global t2_atrasada
    t1_atrasada, t2_atrasada = False, False

    for x, instrucao in enumerate(instrucoes):

        try:
            # Verifica se está T1 está bloqueada, adiciona na lista de atrasadas e continua
            if t1_atrasada:
                # print('instruções atrasadas', instrucoes_atrasadas)
                if executa_instrucao_sem_print(instrucoes_atrasadas[-1], x):  # executa a última instrução atrasada
                    t1_atrasada = False
                    instrucoes_atrasadas.pop()

            if t2_atrasada:
                # print('instruções atrasadas', instrucoes_atrasadas)
                if executa_instrucao_sem_print(instrucoes_atrasadas[-1], x):  # executa a última instrução atrasada
                    t2_atrasada = False
                    instrucoes_atrasadas.pop()

            executou = executa_instrucao_sem_print(instrucao, x)
            if executou is False:
                instrucoes_atrasadas.append(instrucao)

            # DEADLOCK (remove a última instrução atrasada)
            if t1_atrasada and t2_atrasada:
                return True

        except IndexError:
            pass
    return False


def executa_instrucoes(instrucoes):
    """
    Uma vez finalizada a entrada de dados, verifique se a escala de execução que foi informada produz
    deadlocks. Isso pode ser verificado através dos bloqueios que foram feitos em cima de A e B.
    Se há deadlock, mostre na tela quais dados/transações estão envolvidos no deadlock (por exemplo,
    “ocorreu deadlock porque T1 está esperando o dado A que está bloqueado por T2 e T2 está esperando o
    dado B que está bloqueado por T1”).
    Escolha uma das transações como vítima para quebrar a situação de deadlock. Mostre as instruções
    que devem ser desfeitas e qual transação foi escolhida como vítima.
    """
    global t1_atrasada
    global t2_atrasada

    for x, instrucao in enumerate(instrucoes):

        try:
            # Verifica se está T1 está bloqueada, adiciona na lista de atrasadas e continua
            if t1_atrasada:
                # print('instruções atrasadas', instrucoes_atrasadas)
                if executa_instrucao(instrucoes_atrasadas[-1], x):  # executa a última instrução atrasada
                    t1_atrasada = False
                    instrucoes_atrasadas.pop()

            if t2_atrasada:
                # print('instruções atrasadas', instrucoes_atrasadas)
                if executa_instrucao(instrucoes_atrasadas[-1], x):  # executa a última instrução atrasada
                    t2_atrasada = False
                    instrucoes_atrasadas.pop()

            executou = executa_instrucao(instrucao, x)
            if executou is False:
                instrucoes_atrasadas.append(instrucao)

            # DEADLOCK (remove a última instrução atrasada)
            if t1_atrasada and t2_atrasada:
                print('DEADLOCK')
                # Remove a última instrução atrasada
                removida = instrucoes_atrasadas.pop()

                print('\nInstrução removida: ')
                imprimir_instrucoes([removida])

                # Verifica se remover apenas uma transação funcionou, se não chama as instruções de novo sem a removida
                instrucoes.remove(removida)
                if (verifica_deadlock(instrucoes)):
                    executa_instrucoes(instrucoes)


        except IndexError:
            pass


if __name__ == '__main__':
    instrucoes = ler_instrucoes()
    print('\nINSTRUÇÕES: ')
    imprimir_instrucoes(instrucoes)
    executa_instrucoes(instrucoes)
